<?php
  require_once 'vendor/autoload.php';

  function cleanupentries($entry) {
  	$entry = trim($entry);
  	$entry = stripslashes($entry);
  	$entry = htmlspecialchars($entry);

  	return $entry;
  }

  if ( empty($_POST['countdownTimer'])
  || empty($_POST['name'])
  || empty($_POST['email']) ) {
    echo 'Do not try to access here directly';
    exit;
  }

  $formData = [
    'countdownTimer' => cleanupentries($_POST['countdownTimer']),
    'recommenderEmail' => cleanupentries($_POST['recommenderEmail']),
    'name' => cleanupentries($_POST['name']),
    'email' => cleanupentries($_POST['email']),
    'tel' => cleanupentries($_POST['tel']),
    'comActivity' => cleanupentries($_POST['comActivity']),
    'city' => cleanupentries($_POST['city']),
    'comment' => cleanupentries($_POST['comment']),
    'browser' =>  cleanupentries($_POST['browser'])
  ];

  $mail = new PHPMailer;
  $mail->CharSet = "UTF-8";

  $mail->setFrom($formData['email'], $formData['name']);
  $mail->addAddress('ventas@sanherti.com', 'Ventas Sanher Ti');

  $mail->Subject = 'Han enviado datos desde ofertamicredencial.sanherti.com';
  $mail->Body    = '<h2>Datos del formulario:</h2> <br />'
  . '<b>Contador:</b> ' . $formData['countdownTimer'] . '<br /><br />'
  . '<b>E-mail recomendador: </b>' . $formData['recommenderEmail'] . '<br />'
  . '<b>Nombre: </b>' . $formData['name'] . '<br />'
  . '<b>E-mail: </b>' . $formData['email'] . '<br />'
  . '<b>Teléfono: </b>' . $formData['tel'] . '<br />'
  . '<b>Actividad comercial: </b>' . $formData['comActivity'] . '<br />'
  . '<b>Ciudad: </b>' . $formData['city'] . '<br />'
  . '<b>Comentario: </b>' . $formData['comment'] . '<br />'
  . '<b>Navegador: </b>' . $formData['browser'] . '<br />';

  $mail->AltBody    = 'Datos del formulario. '
  . 'Contador: ' . $formData['countdownTimer']
  . 'E-mail recomendador: ' . $formData['recommenderEmail']
  . 'Nombre: ' . $formData['name']
  . 'E-mail: ' . $formData['email']
  . 'Teléfono: ' . $formData['tel']
  . 'Actividad comercial: ' . $formData['comActivity']
  . 'Ciudad: ' . $formData['city']
  . 'Comentario: ' . $formData['comment']
  . 'Navegador: ' . $formData['browser'];


  if( !$mail->send() ) {
    $response = [
      'sended' => false,
      'error' => $mail->ErrorInfo
    ];
    echo json_encode($response);
  } else {
    // Envíarles un correo como respuesta
    $mailResponse = new PHPMailer;
    $mailResponse->CharSet = "UTF-8";

    $mailResponse->setFrom('contacto@sanherti.com', 'Contacto Sanher Ti');
    $mailResponse->addAddress($formData['email'], $formData['name']);
    $mailResponse->Subject = 'Sistema de credencializacion';
    $mailResponse->Body =  'Recibimos tu petición para el sistema de MiCredencial.
                            <br />
                            Te recordamos que la oferta sólo es valida 15 días a partir de ahora.
                            <br />
                            en la brevedad estaras recibiendo la cotizacion del sistema MiCredencial.';
    $mailResponse->AltBody =  'Recibimos tu petición para el sistema de MiCredencial.
                              <br />
                              Te recordamos que la oferta sólo es valida 15 días a partir de ahora.
                              <br />
                              en la brevedad estaras recibiendo la cotizacion del sistema MiCredencial.';
    $mailResponse->send();

    $response = [
      'sended' => true,
    ];
    echo json_encode($response);
  }

?>
